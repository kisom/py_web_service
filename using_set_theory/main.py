#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: kyle isom <coder@kyleisom.net>
# date: 2012-01-26
# license: ISC / public domain dual license
#   (see http://brokenlcd.net/license.txt)
"""
Main example code for 'Using Set Theory' blog post.
"""

import formats
import library
import sample_library

def main():
    my_library = sample_library.get_library()
    my_library.show_titles('books in library:')

    formats.EPUB.show_titles('books in epub format:')
    formats.EPUB.show_titles('books in mobi format:')

    both_formats = library.BookCollection(
            set.intersection(formats.EPUB.books, formats.MOBI.books))
    both_formats.show_titles('books in both epub and mobi format:')


if __name__ == '__main__':
    main()
