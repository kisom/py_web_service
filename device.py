# -*- coding: utf-8 -*-
# author: kyle isom <coder@kyleisom.net>
# date: 2012-01-25
# license: ISC / public domain
"""
Device map for library.
"""

DEVICES = {
        'ipad': ['epub', 'pdf'],
        'kindle': ['mobi', 'pdf'],
        'ipod': ['epub', 'pdf'],
        'nook': ['epub', 'pdf'],
        'kobo': ['epub', 'pdf']
}


