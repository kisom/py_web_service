# -*- coding: utf-8 -*-
# author: kyle isom <coder@kyleisom.net>
# date: 2012-01-27
# license: ISC / public domain
"""
Configuration options for API illustration.
"""

GLOBAL = {
        'DEBUG': True,
        'DEFAULT_PORT': 8080,
        'VERSION': "1.0.2",
}

SETOP = {
        'all': set.intersection,
        'any': set.union,
        'not': set.difference
}

