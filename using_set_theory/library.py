#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: kyle isom <coder@kyleisom.net>
# date: 2012-01-24
# license: isc / public domain (http://www.brokenlcd.net/license.txt)
"""
Library illustration for post "Using Set Theory"
    (http://kisom.github.com/blog/2012/01/24/using-set-theory/)

This contains the Library class.

Book summaries come from their descriptions on O'Reilly's website
    (http://www.oreilly.com)
or on Manning's site (in the case of The Joy of Clojure):
    (http://www.manning.com/fogus/)
"""

SUPPORTED_FORMATS = ['epub', 'mobi', 'pdf']


class Book:
    """
    Represents a book, with title, author, and summary text fields. A book
    should be given a list of formats supported as a dictionary in the form
    {fmt: True}, and optionally a list of tags.
    """
    title = None
    author = None
    summary = None
    formats = None
    tags = None

    def __init__(self, title, author, summary, formats):
        """Initalise a new book. The format shoud be a dictiontary in
        the form { 'epub': True } where each key is a format that we
        have the book in."""

        self.title = unicode(title)
        self.author = [unicode(auth) for auth in author]
        self.summary = unicode(summary)

        assert(not False in [fmt in SUPPORTED_FORMATS for fmt in formats])
        self.formats = [unicode(fmt) for fmt in formats]

    def __str__(self):
        """
        Return string representation of a book.
        """
        out = "%s\n\tby %s\n\t%s\n\tformats: %s"
        out = out % (self.title, ', '.join(self.author),
                     self.summary, ', '.join(self.formats))
        return out

    def json(self):
        """
        Return a book as a dictionary suitable for passing to json.dumps.
        """

        json = {
                u'title': self.title,
                u'authors': self.author,
                u'summary': self.summary,
                u'formats': self.formats
        }

        return json


class BookCollection:
    """Representation of a collection of books. Internally, they are stored
    as a set. It's main utility is in its helper methods that make accessing
    the books easier."""

    def __init__(self, books, book_filter=None):
        """Instantiate a collection of books. It expects a collection of
        books, e.g. a list or set, and optionally takes a filter to
        only put some of the books into the collection."""

        if book_filter:
            self.books = set([book for book in books if book_filter(book)])
        else:
            self.books = set(books)

    def __len__(self):
        return len(self.books)

    def show_titles(self, description=None):
        """Print a list of titles in the collection. If the description
        argument is supplied, it is printed first and all the books are
        printed with a preceding tab."""
        if description:
            print description
            fmt = '\t%s'
        else:
            fmt = '%s'

        for book in self.books:
            print fmt % (book.title, )

    def get_titles(self):
        """Return a list of titles in the collection."""
        return [book.title for book in self.books]

book_as_json = lambda books : [book.json for book in books]
