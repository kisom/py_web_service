#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: kyle isom <coder@kyleisom.net>
# date: 2012-01-27
# license: ISC / public domain dual-license
"""
Flask-based server for the web API.
"""


import flask
import os
import simplejson
import using_set_theory.library
from config import GLOBAL, SETOP
from device import DEVICES
from using_set_theory.sample_library import get_library

app = flask.Flask(__name__)

###########################################################################
#                       utility functions                                 #
###########################################################################

get_set_op = lambda sel : SETOP[get_select(sel)]
books_as_json = lambda books: [book.json() for book in books]
books_in_format = lambda fmt, books: [book for book in books 
                                           if fmt in book.formats]

def args_to_dict(args):
    """
    Build a dictionary from the tuple list.
    """
    args = {}
    for arg in args:
        args[arg] = args[arg]
    if not 'select' in args:
        args['select'] = 'all'
    return args 

def get_select(select):
    """
    Returns an appropriate select mode for building subsets.
    """
    if not select in SETOP:
        return 'all'
    return select

def get_formats(books):
    book_formats = [book.formats for book in books]

def text_out(body):
    """Build a plain text response."""
    resp = flask.make_response(body, 200)
    resp.headers['content-type'] = 'text/plain'
    return resp

def json_out(body):
    """Build a JSON response."""
    resp = flask.make_response(simplejson.dumps(body), 200)
    resp.headers['content-type'] = 'application/json'
    return resp

def get_port():
    """Decide which port to run on."""
    if os.getenv('PORT'):
        return int(os.getenv('PORT'))
    else:
        return GLOBAL['DEFAULT_PORT']

def get_format_list():
    books = get_library().books
    formats = set([book_format for book in books 
                               for book_format in book.formats])
    return list(formats)

def filter_formats(args, select, formats):
    format_list = args['formats'].split(',')
    book_list = []
    for fmt in format_list:
        book_list.append(books_in_format(fmt, get_library().books))
    
    return map(books_as_json, apply(get_set_op(select), book_list))

###########################################################################
#                               routes                                    #
###########################################################################
@app.route('/')
def root():
    out = ("This is an illustrative web API for the book library. I " +
          "wrote this for a blog post on using set theory:\n" +
          "\thttp://kisom.github.com/blog/2012/01/??/using-set-theory/" +
          "\n\n\nthe source is on github: " +
          "\n\tgit clone git://github.com/kisom/clj_web_service.git\n" +
          "\nGET /help for usage")
    return text_out(out)

@app.route('/help')
def help():
    out = open('static/help').read()
    return text_out(out)

@app.route('/api')
def api():
    return json_out({'version': GLOBAL['VERSION']})

@app.route('/api/books')
def api_books():
    books = get_library().books
    return json_out(books_as_json(books))

@app.route('/api/books/titles')
def api_books_titles():
    books = get_library().books
    return json_out([book.title for book in books])

@app.route('/api/books/title/<title>')
def api_books_title(title):
    books = get_library().books
    out = set([book for book in books if book.title == title])
    return json_out(books_as_json(out))

@app.route('/api/books/formats')
def api_books_formats():
    books = get_library().books
    args = args_to_dict(flask.request.args)

    if not flask.request.query_string or not 'formats' in args:
        return json_out(get_format_list())
    else:
        return json_out(filter_formats)

@app.route('/api/books/format/<fmt>')
def api_books_format(fmt):
    books = get_library().books
    return json_out([book.json() for book in books if fmt in book.formats])

@app.route('/api/authors')
def api_authors():
    authors = set([author for book in get_library().books
                          for author in book.author])
    return json_out(list(set(authors)))

@app.route('/api/author/<author>')
def api_author_author(author):
    books = get_library().books
    out = [book.json() for book in books if author in book.author]
    return json_out(out)

@app.route('/api/devices')
def api_devices():
    return json_out(DEVICES.keys())

@app.route('/api/devices/<device>')
def api_devices_device(device):
    device_set = []
    books = get_library().books

    if not device in DEVICES:
        return json_out("not found")

    for fmt in DEVICES[device]:
        fmt_list = [book for book in books if fmt in book.formats]
        device_set.append(set(list(fmt_list)))

    books = apply(set.union, device_set)
    return json_out(list(books_as_json(books)))
    

def main():
    app.run(debug=GLOBAL['DEBUG'], port=get_port())

if __name__ == '__main__':
    main()
