# -*- coding: utf-8 -*-
# author: kyle isom <coder@kyleisom.net>
# date: 2012-01-24
# license: ISC / public domain (http://www.brokenlcd.net/license.txt)
"""
Contains the sample library.
"""

from library import Book, BookCollection

def get_library():
    """
    Load the sample dataset.
    """
    return BookCollection(set([
        Book("Natural Language Processing with Python",
            ["Steven Bird", "Ewan Klein", "Edward Loper"],
            "A highly accessible introduction to natural language processing.",
            ['mobi']),
        Book("Learning OpenCV", ["Gary Bradski", "Adrian Kaehler"],
            "Puts you in the middle of the rapidly expanding field of " +
            "computer vision", ['pdf']),
        Book("Code Complete", ["Steve McConnell"],
            "McConnell synthesizes the most effective techniques and " +
            "must-know principles into clear, pragmatic guidance.",
            ['epub', 'mobi', 'pdf']),
        Book("Mastering Algorithms with C", ["Kyle Loudon"],
            "Offers robust solutions for everyday programming tasks, " +
            "and provides all of the necessary information to " +
            "understand and use common programming techniques.",
            ['pdf']),
        Book("The Joy of Clojure", ["Michael Fogus", "Chris Houser"],
            "Goes beyond just syntax to show you how to write fluent " +
            "and idiomatic Clojure code.", ['epub', 'pdf']),
        Book("Mining the Social Web", ["Matthew A. Russel"],
            "You'll learn how to combine social web data, analysis " +
            "techniques, and visualization to help you find what you've " +
            "been looking for in the social haystack.",
            ['epub', 'pdf']),
        Book("Algorithms in a Nutshell", 
            ["George T. Heineman", "Gary Pollice", "Stanley Selkow"],
            "Describes a large number of existing algorithms for solving " +
            "a variety of problems.",
            ['pdf']),
        Book("Introduction to Information Retrieval",
            ["Christopher D. Manning", "Prabhakar Raghavan", "Hunrich Schutze"],
            "Teaches web-era information retrieval, including web search " +
            "and the related areas of text classification and text " +
            "clustering from basic concepts.",
            ['mobi']),
        Book("Network Security with OpenSSL",
            ["John Viega", "Matt Messier", "Pravir Chandra"],
            "Enables developers to use this protocol much more effectively.",
            ['pdf']),
        Book("RADIUS", ["Jonathan Hassel"],
            "Provides a complete, detailed guide to the underpinnings " +
            "of the RADIUS protocol.",
            ['pdf'])]))

