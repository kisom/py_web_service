#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: kyle isom <coder@kyleisom.net>
# license: ISC / public domain
"""
Test suite for web API illustration.
"""

import datetime
import os
import re
import requests
import simplejson
import sys
import unittest

from device import DEVICES
from using_set_theory.sample_library import get_library
from using_set_theory.formats import EPUB, MOBI, PDF

books_to_json = lambda books : [book.json() for book in books]

SERVER = 'http://127.0.0.1:%d' % (int(os.getenv('PORT')) 
                                  if os.getenv('PORT')
                                  else 8080)

FORMAT_MAP = { 'epub': books_to_json(EPUB.books),
               'mobi': books_to_json(MOBI.books),
               'pdf': books_to_json(PDF.books) }

url = lambda path : '%s/%s' % (SERVER, path.lstrip('/'))
books_in_format = lambda fmt, books: [book for book in books 
                                           if fmt in book.formats]

def get_request(req):
    body = unicode(req.content).encode('utf-8')
    return simplejson.loads(body)

class ServerTests(unittest.TestCase):

    def test_help(self):
        disk_size = len(open('static/help').read())
        
        api_help = url('/help')
        req = requests.get(api_help)

        http_size = len(req.content)

        self.assertEquals(http_size, disk_size)

    def test_version(self):
        version_regex = r'^\d+\.\d+(\.\d+)?$'

        req = requests.get(url('/api'))
        version = get_request(req)
        self.assertIn('version', version)
        self.assertTrue(re.match(version_regex, version['version'])) 

    def test_books(self):
        books_ref = get_library().books
        req = requests.get(url('/api/books'))
        books = get_request(req)

        self.assertEquals(10, len(books))

    def test_books_titles(self):
        books_ref = get_library().books
        title_ref = set([book.title for book in books_ref])
        
        req = requests.get(url('/api/books/titles'))
        titles = set(get_request(req))

        self.assertFalse(set.difference(title_ref, titles))
        self.assertFalse(set.difference(titles, title_ref))

    def test_books_title_title(self):
        books_ref = get_library().books
        title_ref = set([book.title for book in books_ref])

        for title in title_ref:
            req = requests.get(url('/api/books/title/' + title))
            book_ref = [book.json() for book in books_ref 
                                    if book.title == title]

            book = get_request(req)
            self.assertEqual(book, book_ref)
    
    def test_books_formats(self):
        books_ref = get_library().books
        format_ref = set([fmt for book in books_ref
                              for fmt in book.formats])

        req = requests.get(url('/api/books/formats'))
        formats = set(get_request(req))
        self.assertEqual(formats, format_ref)

    def test_books_formats_format(self):
        books_ref = get_library().books
        format_ref = set([fmt for book in books_ref for fmt in book.formats])

        for fmt in format_ref:
            req = requests.get(url('/api/books/format/' + fmt))
            format_books = get_request(req)

            self.assertEqual(sorted(format_books), sorted(FORMAT_MAP[fmt]))

    def test_authors(self):
        authors_ref = []
        authors_ref = set([author for book in get_library().books
                                  for author in book.author])
                       
        req = requests.get(url('/api/authors'))
        authors = set(get_request(req))
        self.assertEqual(len(authors), len(authors_ref))
        self.assertFalse(authors.difference(authors_ref))
        self.assertFalse(authors_ref.difference(authors))

    def test_authors_author(self):
        authors_ref = []
        authors_ref = set([author for book in get_library().books
                                  for author in book.author])
        books_ref = get_library().books

        for author in authors_ref:
            author_books_ref = [book.json() for book in books_ref
                                            if author in book.author]
            req = requests.get(url('/api/author/' + author))
            author_books = get_request(req)
            self.assertEqual(sorted(author_books), sorted(author_books_ref))

    def test_devices(self):
        req = requests.get(url('/api/devices'))
        devices = get_request(req)
        self.assertEqual(len(devices), len(DEVICES))
        self.assertEqual(sorted(devices), sorted(DEVICES.keys()))

    def test_devices_device(self):
        device_list = DEVICES.keys()
        books_ref = get_library().books
        for device in device_list:
            device_set = []
            req = requests.get(url('/api/devices/' + device))
            device_books = get_request(req)

            for fmt in DEVICES[device]:
                format_list = set(books_in_format(fmt, books_ref))
                device_set.append(format_list)

            device_set = books_to_json(apply(set.union, device_set))
            self.assertEqual(sorted(device_books), sorted(device_set))
 
def main(verbosity=2):
    start = datetime.datetime.utcnow()
    suite   = unittest.TestSuite()
    loader  = unittest.TestLoader()
    suite.addTests(loader.loadTestsFromTestCase(ServerTests))
    unittest.TextTestRunner(verbosity=verbosity).run(suite)
    return datetime.datetime.utcnow() - start

if __name__ == '__main__':
    print 'testing against server: %s' % (SERVER, )
    if len(sys.argv) > 1:
        n = int(sys.argv[1])
        verbosity = 0
    else:
        n = 1
        verbosity = 2
    times = []
    for i in range(n):
        sys.stderr.write('test number: %d / %d\n' % (i, n))
        times.append(main(verbosity))

    print "average run time: %s" % (str((reduce(lambda x,y: x + y, times)) / n))

