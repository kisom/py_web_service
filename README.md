# py_web_service
This is an example web service to illustrate an article on using set theory.


# Dependencies
You need the following:

* requests
* simplejson
* Flask

`sudo pip install requests simplejson flask` if you have pip, or
`sudo easy_install requests simplejson flask`. (You might need to
grab the Python setup tools, which you will need to look at your
package manager for).

# Using
Run the server with 

    python ./server.py

Test it with

    python ./tests.py

You can use the `PORT` environment variable to change the server's
port; it defaults to `8080`.


