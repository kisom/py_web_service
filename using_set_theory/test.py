#!/usr/bin/env python
# -*- coding: utf-8 -*-
# author: kyle isom <coder@kyleisom.net>
# date: 2012-01-26
# license: ISC / public domain dual license
#   (see http://brokenlcd.net/license.txt)
"""
Unit tests for using set theory example code.
"""

import formats
import library
import sample_library
import unittest


MY_LIBRARY = sample_library.get_library()

class UsingSetTheoryTests(unittest.TestCase):
    """
    Class holding all of the tests.
    """

    def LibraryIntegrity(self):
        """
        Ensure the sample library loads properly.
        """
        self.assertEqual(10, len(MY_LIBRARY))


    def SubsetTests(self):
        """
        Ensure the integrity of the subsets.
        """
        self.assertEqual(3, len(formats.EPUB))
        self.assertEqual(3, len(formats.MOBI))


    def IntersectionTest(self):
        """
        Ensure the intersection returns as expected.
        """
        both_formats = library.BookCollection(
            set.intersection(formats.EPUB.books, formats.MOBI.books))
 
        self.assertEqual(1, len(both_formats))

if __name__ == '__main__':
    suite   = unittest.TestSuite()
    loader  = unittest.TestLoader()
    suite.addTests(loader.loadTestsFromTestCase(UsingSetTheoryTests))
    unittest.TextTestRunner(verbosity = 2).run(suite)

