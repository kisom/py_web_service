# -*- coding: utf-8 -*-
# author: kyle isom <coder@kyleisom.net>
# date: 2012-01-24
# license: ISC / public domain (http://www.brokenlcd.net/license.txt)
"""
Several illustrations for Using Set Theory.
"""

import library
import sample_library

# my_library is our superset
MY_LIBRARY = sample_library.get_library()

# build our filters
IS_EPUB = lambda book: 'epub' in book.formats
IS_MOBI = lambda book: 'mobi' in book.formats
IS_PDF = lambda book: 'pdf' in book.formats

# build the subsets
EPUB = library.BookCollection(MY_LIBRARY.books, IS_EPUB)
MOBI = library.BookCollection(MY_LIBRARY.books, IS_MOBI)
PDF = library.BookCollection(MY_LIBRARY.books, IS_PDF)
